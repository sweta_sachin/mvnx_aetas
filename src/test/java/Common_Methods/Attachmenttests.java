package Common_Methods;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;

public class Attachmenttests extends CommonMethods
{

	
///////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************** Method to click attachment *************************************
public void ClickAttachment() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);
	
	WebElement attachment= driver.findElement(By.id("B8982416461177513"));
	
	Boolean name= attachment.isDisplayed();
	
	if(name==true)	{
		
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(attachment).doubleClick().build().perform();
		
		Thread.sleep(1500);
	}
}

//****************************** Method to select document category*******************************
public void Select_document_Category() throws InterruptedException
{
	Select dropdown= new Select(driver.findElement(By.id("P59_DOCG_UID")));
	
	dropdown.selectByVisibleText("Account");
	
	Thread.sleep(500);
}

//******************************  Method to select document type *******************************	
public void Select_document_Type() throws InterruptedException
{
	Select dropdown= new Select(driver.findElement(By.id("P59_DOC_DOCT_UID")));
	
	dropdown.selectByVisibleText("Proof of Identity");
	
	Thread.sleep(500);
}

//************************************* Method to enter name ***********************************	
public void Enter_Name()
{
	driver.findElement(By.id("P59_DOC_NAME")).clear();
	
	driver.findElement(By.id("P59_DOC_NAME")).sendKeys("test123");
	
}
	
//	******************************** Method to select file ****************************************
public void Select_file() throws InterruptedException
{
	Thread.sleep(500);

	WebElement file= driver.findElement(By.id("P59_FILE"));
	
	file.sendKeys("C:\\Users\\Sweta\\Desktop\\test doc.txt");
	
	Thread.sleep(500);
}

//****************************** Method to click attach ******************************************
public void Click_attach()
{
	try {
	
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		
		driver.findElement(By.id("B15126227968958324")).click();
	
	}catch(NoSuchElementException e) {
	
		driver.findElement(By.id("B15126227968958324")).click();
	}
}
//****************************** Method to verify attachment **************************************	
/*	public void Verify_Attachment()
{
WebElement table1= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[2]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]"));
System.out.println(table1.getAttribute());


} */
//******************************** Method to cancel attachment *************************
public void cancel_attachment()
{
	try {
		
		WebElement cancel= driver.findElement(By.linkText("Cancel"));
		
		Actions act= new Actions (driver);
		
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		
		act.moveToElement(cancel).doubleClick().build().perform();
	
	}catch(NoSuchElementException e) {
	
		WebElement cancel= driver.findElement(By.linkText("Cancel"));
		
		Actions act= new Actions (driver);
		
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		
		act.moveToElement(cancel).doubleClick().build().perform();
	}
	
	driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	
	Boolean edit=	driver.findElement(By.linkText("Edit Customer")).isDisplayed();
	
	Assert.assertTrue(edit);
}

//******************************* Method to view attachments ******************************
public void view_attachment()
{
	WebElement table1= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[2]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]"));

	Boolean viewtable= table1.isDisplayed();
	
	Assert.assertTrue(viewtable);
}

//******************************* Method to click edit attachment ******************
public void click_edit() throws InterruptedException
{
	try {
	
		Thread.sleep(1500);
		
		WebElement edit= driver.findElement(By.xpath("//tr[2]//td[1]//a[1]//img[1]"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(edit).doubleClick().build().perform();
		
	}catch(StaleElementReferenceException e) {
	
		WebElement edit= driver.findElement(By.xpath("//tr[2]//td[1]//a[1]//img[1]"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(edit).doubleClick().build().perform();
		
	}
}

//***************************** Method to attach new file using edit attachment ***********
public void Attach_edit() throws InterruptedException
{
	Thread.sleep(1500);
	
	WebElement edit= driver.findElement(By.xpath("//tr[2]//td[1]//a[1]//img[1]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(edit).doubleClick().build().perform();
	
	driver.findElement(By.id("P59_DOC_NAME")).clear();
	
	driver.findElement(By.id("P59_DOC_NAME")).sendKeys("new name");
	
	Select_file();
	
	WebElement click= driver.findElement(By.linkText("Apply Changes"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(click).doubleClick().build().perform();
	
}
//***************************** Method to delete attachment ********************************
public void Delete_Attachment() throws InterruptedException
{
	try {

		click_edit();
		
		Thread.sleep(1500);
		
		WebElement delete =driver.findElement(By.id("B15126405025958324"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(delete).doubleClick().build().perform();
		
		Thread.sleep(1000);
		
		Alert alert= driver.switchTo().alert();
		
		alert.accept();
	
	}catch(UnhandledAlertException e){
	
		Alert alert= driver.switchTo().alert();
		
		alert.accept();
	
	}catch( NoAlertPresentException e){
	
		Alert alert= driver.switchTo().alert();
		
		alert.accept();
	}
}
//****************************** Method to cancel deletion of attachment **************************
public void cancel_deletion_attachment() throws InterruptedException
{
	try {
		
		click_edit();
		
		Thread.sleep(1000);
		
		WebElement delete =driver.findElement(By.id("B15126405025958324"));
		
		Actions act= new Actions(driver);
		
		act.moveToElement(delete).doubleClick().build().perform();
		
		Thread.sleep(1000);
		
		Alert alert= driver.switchTo().alert();
		
		alert.dismiss();
	
	}catch(UnhandledAlertException | NoAlertPresentException e)	{
		
		Alert alert= driver.switchTo().alert();
		
		alert.dismiss();
	}
  }
}
