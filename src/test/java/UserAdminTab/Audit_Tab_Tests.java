package UserAdminTab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Audit_Tab_Tests  extends Common_Methods.CommonMethods{
	
	public static String Group= "security service group1";
		
	public static String Uname= "new name Maskelar";
		
	@BeforeClass()
	public void CRM() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_UserAdmin();	
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() 
	{
	    driver.quit();
	}
	
	@Test
	public void a_view_Audit_Tab()
	{
		view_AuditTab();
	}
	
	@Test
	public void b_Click_Audit_Tab() throws InterruptedException
	{
		Thread.sleep(500);
		click_AuditTab();
	}
	
	@Test
	public void c_SearchReportBy_MSISDN() throws InterruptedException
	{
		Thread.sleep(500);
		SearchReportBy_MSISDN();
	}
	
	
	@Test
	public void d_SearchReportBy_UserName() throws InterruptedException
	{
		Thread.sleep(500);
		SearchReportBy_UserName();
	}
	
	@Test
	public void e_SearchReportBy_AuditType() throws InterruptedException
	{
		Thread.sleep(500);
		SearchReportBy_AuditType();
	}
	
	@Test
	public void f_SearchReportBy_Application() throws InterruptedException
	{
		Thread.sleep(500);
		SearchReportBy_Application();
	}
	

	@Test
	public void g_GeneralSearch() throws InterruptedException
	{
		Thread.sleep(500);
		General_Search();	
	}
	
	@Test
	public void h_Filter_report() throws InterruptedException
	{
		Thread.sleep(1500);
		View_filtered_report();
	}
	
	@Test
	public void i_Search_For_Primary_report() throws InterruptedException
	{
		Thread.sleep(5000);
		reset();
		search_by_PrimaryReports();
	}
	
	@Test
	public void j__Save_Report() throws InterruptedException
	{
		Thread.sleep(2500); 
		Save_report();
	}
	
	@Test
	public void k_Search_by_privateReport() throws InterruptedException
	{
		Thread.sleep(2500); 
		reset();
		search_by_private_report();
	}
	
	@Test
	public void l_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(2500); 
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","audit_report.csv"));
	}

	@Test
	public void m_Send_Subscription_Email() throws InterruptedException
	{
		Thread.sleep(2500); 
		get_subscription();
		
	}
	
	@Test
	public void x_reset_test() throws InterruptedException
	{
		Thread.sleep(3000); 
		reset();
		
	}
	
	@Test
	public void z_flashBackTest() throws InterruptedException
	{
		Thread.sleep(3000); 
		flashback_filter();
	}
	
		
//////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Audit tab ******************************
public void view_AuditTab()
{
	WebElement aging= driver.findElement(By.linkText("Audit"));
	
	Boolean Aging= aging.isDisplayed();
	
	Assert.assertTrue(Aging);
}

//********************************* Method to click Audit tab ******************************
public void click_AuditTab()
{
	WebElement aging= driver.findElement(By.linkText("Audit"));

	Actions act= new Actions(driver);
	
	act.moveToElement(aging).click().build().perform();
	
	WebElement verify= driver.findElement(By.id("P29_FROM_DATE"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}

//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
{
	 expvalue= "Apex Login";
	  col= "Type"; 
	
	  view_filtered_report();

	  try {
	  
		  //Verify report
		  
		  Thread.sleep(300);
		  
		  String verifyreport= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[2]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[4]")).getText();
		  
		  Assert.assertEquals(verifyreport, expvalue);
	 
	  }catch(Exception e){
		
		  // Verify report
			
		  Thread.sleep(300);
			
		  String verifyreport= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[2]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[4]")).getText();
			
		  Assert.assertEquals(verifyreport, expvalue);	
  }
		
}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "1. Test report"; 
		
	save_report();

	// Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);

}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();
		
	// verify 
	try {
	
		Thread.sleep(300);
		
		String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
		
		System.out.println(text);
		
		Assert.assertEquals(text, verifyreport);
		
	}catch(Exception e) {
	
		Thread.sleep(300);
		
		String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
		
		System.out.println(text);
		
		Assert.assertEquals(text, verifyreport);	
		
	}

}
//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
{
	select_primary_report();

	Thread.sleep(300);
	
	try {
	
		WebElement primaryreport= driver.findElement(By.id("6677720189934829"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
		
	}catch(Exception e) {
	
		WebElement primaryreport= driver.findElement(By.id("6677720189934829"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
		
	}
}

//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
{
	Reset();
		
	// Verify report
	
	WebElement primaryreport= driver.findElement(By.id("6677720189934829"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
}


//*********************************************** Method to click Search ***********************************
public void Click_Search() throws InterruptedException 
	{
	
	// Click search
	
	WebElement Search = driver.findElement(By.id("P29_GO"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(Search).click().build().perform();	
	
	Thread.sleep(500);
}

//************************************************ Method to get report for general search ******************
public void General_Search() throws InterruptedException
{
	Click_Search();
	
	// Verify report

	WebElement primaryreport= driver.findElement(By.id("6677720189934829"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
}

//********************************* Method to search report by user name **********************************
public void SearchReportBy_UserName() throws InterruptedException
{
	// Select user name
	
	Select username= new Select(driver.findElement(By.id("P29_USER")));
	
	username.selectByVisibleText(Uname);
	
	Click_Search();
	
	Thread.sleep(1000);
	
	Select username1= new Select(driver.findElement(By.id("P29_USER")));
	
	username1.selectByVisibleText("-- Select --");
	
	//Verify report

	Thread.sleep(300);
	
	String verifyreport= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[2]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[3]")).getText();
	
	Assert.assertEquals(verifyreport, Uname);

}

//********************************* Method to search report by Audit type **********************************
public void SearchReportBy_AuditType() throws InterruptedException
{
	
	// Select Audit type
	
	Select AuditType= new Select(driver.findElement(By.id("P29_UAT_NAME")));
	
	AuditType.selectByVisibleText("Apex Login");
	
	Click_Search();
	
	Thread.sleep(1000);
	
	Select username1= new Select(driver.findElement(By.id("P29_UAT_NAME")));
	
	username1.selectByVisibleText("-- Select --");

	//Verify report
	
	Thread.sleep(300);
	
	String verifyreport= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[2]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[4]")).getText();
	
	Assert.assertEquals(verifyreport, "Apex Login");

}

//********************************* Method to search report by Application **********************************
public void SearchReportBy_Application() throws InterruptedException
{
	// Select application
		
	Select Application= new Select(driver.findElement(By.id("P29_APPLICATION")));
	
	Application.selectByVisibleText("01 - User Administration");
	
	Click_Search();
	
	Thread.sleep(1000);
	
	Select username1= new Select(driver.findElement(By.id("P29_APPLICATION")));
	
	username1.selectByVisibleText("-- Any Application --");

	//Verify report
	
	Thread.sleep(300);
	
	String verifyreport= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[2]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[6]")).getText();
	
	Assert.assertEquals(verifyreport, "01 - User Administration");
}


//********************************* Method to search report by MSISDN **********************************
public void SearchReportBy_MSISDN() throws InterruptedException
{
	// enter MSISDN
		
	WebElement MSISDN= driver.findElement(By.id("P29_MSISDN"));
	
	MSISDN.clear();
	
	MSISDN.sendKeys(msisdn);
	
	Click_Search();
	
	Thread.sleep(2000);
	
	driver.findElement(By.id("P29_MSISDN")).clear();
	 
	//Verify report
	
	Thread.sleep(300);
	
	String verifyreport= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[2]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[8]")).getText();
	
	Assert.assertEquals(verifyreport, msisdn);
  }
}
