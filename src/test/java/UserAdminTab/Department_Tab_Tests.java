package UserAdminTab;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Department_Tab_Tests  extends Common_Methods.CommonMethods{
	
	   @BeforeClass()
	public void CRM() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_UserAdmin();	
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() 
	{
	    driver.quit();
	}
	
	@Test
	public void a_view_Department_Tab()
	{
		view_DepartmentTab();
	}
	
	@Test
	public void b_Click_Department_Tab() throws InterruptedException
	{
		Thread.sleep(500);
		click_DepartmentTab();
	}
	
	
	@Test
	public void c_Create_Department() throws InterruptedException
	{
		Thread.sleep(500); 
		create_department();
	}
	
	@Test
	public void d_verify_created() throws InterruptedException
	{
		Thread.sleep(500); 
		VerifyCreated();
	}
	
	@Test
	public void e_Edit_Department() throws InterruptedException
	{
		Thread.sleep(1500); 
		Edit_department();
	}
	
	@Test
	public void f_verify_Edited() throws InterruptedException
	{
		Thread.sleep(1500); 
		VerifyEdited();
	}
	
	@Test
	public void g_AddUsers() throws InterruptedException
	{
		Thread.sleep(1500); 
		Add_User();
	}
	
	@Test
	public void h_Verify_Added_User() throws InterruptedException
	{
		Thread.sleep(1500); 
		Verify_added_users();
	}
	
	@Test
	public void i_RemoveUsers() throws InterruptedException
	{
		Thread.sleep(1500); 
		Remove_User();
	}
	
	@Test
	public void j_Verify_Removed_User() throws InterruptedException
	{
		Thread.sleep(1500); 
		Verify_Removed_users();
	}
	
	
	@Test
	public void k_Filter_report() throws InterruptedException
	{
		Thread.sleep(500);
		View_filtered_report();
	}
	
	@Test
	public void l_Search_For_Primary_report() throws InterruptedException
	{
		Thread.sleep(500);
		
		search_by_PrimaryReports();
	}
	
	@Test
	public void m__Save_Report() throws InterruptedException
	{
		Thread.sleep(500); 
		Save_report();
	}
	
	@Test
	public void n_Search_by_privateReport() throws InterruptedException
	{
		Thread.sleep(500); 
		search_by_private_report();
	}
	
	@Test
	public void z_reset_test() throws InterruptedException
	{
		Thread.sleep(500); 
		reset();
		
	}

	@Test
	public void p_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","departments.csv"));
	}

	@Test
	public void q_Send_Subscription_Email() throws InterruptedException
	{
		Thread.sleep(300);
		get_subscription();
		
	}
	
	@Test
	public void r_Delete_Department() throws InterruptedException
	{
		Thread.sleep(1500); 
		Delete_department();
	}
	
	@Test
	public void s_verify_Deleted() throws InterruptedException
	{
		Thread.sleep(1500); 
		VerifyDeleted();
	}
	
	@Test
	public void t_flashBackTest() throws InterruptedException
	{
		Thread.sleep(300);
		flashback_filter();
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view DEpartment tab ******************************
public void view_DepartmentTab()
{
	WebElement aging= driver.findElement(By.linkText("Departments"));
	
	Boolean Aging= aging.isDisplayed();
	
	Assert.assertTrue(Aging);
}

//********************************* Method to click Department tab ******************************
public void click_DepartmentTab()
{
	WebElement aging= driver.findElement(By.linkText("Departments"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(aging).click().build().perform();
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}

//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
{
	   expvalue= "new name";
	   col= "Name";
	
	   view_filtered_report();

	try {
		
		// Verify report
		
		Thread.sleep(300);
		
		String verifyreport= driver.findElement(By.xpath("//tr[@class='even']//td[contains(text(),'new name')]")).getText();
		
		Assert.assertEquals(verifyreport, expvalue);
		
	}catch(Exception e)	{
	
		// Verify report
		
		Thread.sleep(300);
		
		String verifyreport= driver.findElement(By.xpath("//tr[@class='even']//td[contains(text(),'new name')]")).getText();
		
		Assert.assertEquals(verifyreport, expvalue);	
		
	}
	
	//Remove extra filters
	
	WebElement filters= driver.findElement(By.xpath("//tr[2]//td[4]//a[1]//img[1]"));
	
	Actions act1 = new Actions(driver);
	
	act1.moveToElement(filters).click().build().perform();
}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "2. Test report"; 
	
	save_report();

	//Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);

	}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();

	//verify 
	
	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
}

//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
{
	select_primary_report();
	
	Thread.sleep(300);
	
	WebElement primaryreport= driver.findElement(By.id("8369207139092751"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
}

//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
{
	Reset();

	//Verify report
	
	WebElement primaryreport= driver.findElement(By.id("8369207139092751"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
}

//********************************************* Method to create department ************************************
public void create_department() throws InterruptedException
{
   // click create 
	
	WebElement create = driver.findElement(By.id("B8369625989092758"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(create).click().build().perform();	
	
	Thread.sleep(500);

	//Enter name
	
	Thread.sleep(300);
	
	WebElement name= driver.findElement(By.id("P15_UD_NAME"));
	
	name.clear();
	
	name.sendKeys( Newname);
	
	// Click create
	
	WebElement Create = driver.findElement(By.id("B8366130320092717"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(Create).click().build().perform();	
	
	Thread.sleep(500);
	
}

//********************************************* Method to verify created department ***************************
public void VerifyCreated() throws InterruptedException
{
	Thread.sleep(500);

	WebElement search= driver.findElement(By.id("apexir_SEARCH"));
	
	search.clear();
	
	search.sendKeys(Newname);
	
	//Click Go
	
	WebElement go = driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(go).click().build().perform();	
	
	Thread.sleep(500);
	
	String text= driver.findElement(By.xpath("//tr[@class='even']//td[2]")).getText();
	
	Assert.assertEquals(text, Newname);	
}

//********************************************* Method to edit department ************************************
public void Edit_department() throws InterruptedException
{
 // click edit
	
	WebElement edit = driver.findElement(By.xpath("//tr[@class='even']//td[1]//a[1]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(edit).click().build().perform();	
	
	Thread.sleep(500);
	
	//Enter name
	
	Thread.sleep(300);
	
	WebElement name= driver.findElement(By.id("P15_UD_NAME"));
	
	name.clear();
	
	name.sendKeys( Newname);
	
	// Click create
	
	WebElement Create = driver.findElement(By.id("B8366202756092717"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(Create).click().build().perform();	
	
	Thread.sleep(500);
}

//********************************************* Method to verify edited department ***************************
public void VerifyEdited() throws InterruptedException
{
	Thread.sleep(500);
	
	WebElement search= driver.findElement(By.id("apexir_SEARCH"));
	
	search.clear();
	
	search.sendKeys(Newname);
	
	//Click Go
	
	WebElement go = driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(go).click().build().perform();	
	
	Thread.sleep(500);
	
	String text= driver.findElement(By.xpath("//tr[@class='even']//td[2]")).getText();
	
	Assert.assertEquals(text, Newname);	
}

//******************************************* Method to click add/remove users ******************************************
public void Click_AddRemove() throws InterruptedException
{
	// Click Add/Remove users
	
	WebElement add = driver.findElement(By.xpath("//tr[@class='even']//a[contains(text(),'Add/Remove')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(add).click().build().perform();	
	
	Thread.sleep(500);	
}


//*************************************** Method to add user *******************************************
public void Add_User() throws InterruptedException
{
	Click_AddRemove();
	
	// Add user

	WebElement adduser = driver.findElement(By.xpath("//option[contains(text(),'abcd xyz')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(adduser).doubleClick().build().perform();	
	
	Thread.sleep(1500);	
	
	//Click Submit
	
	WebElement submit = driver.findElement(By.id("B8408515842282545"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(submit).click().build().perform();	
	
	Thread.sleep(500);	
		
  }

//************************************** Method to verify added user ***************************************
public void Verify_added_users() throws InterruptedException
{
	Click_AddRemove();
	
	Thread.sleep(500);	
	
	Boolean user= driver.findElement(By.xpath("//option[contains(text(),'abcd xyz')]")).isDisplayed();
	
	driver.navigate().back();
	
	Assert.assertTrue(user);
	
}

//*************************************** Method to remove user *******************************************
public void Remove_User() throws InterruptedException
{
	Click_AddRemove();
	
	// Add user
	
	WebElement removeuser = driver.findElement(By.xpath("//option[contains(text(),'abcd xyz')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(removeuser).doubleClick().build().perform();	
	
	Thread.sleep(500);	
		
	//Click Submit
	
	WebElement submit = driver.findElement(By.id("B8408515842282545"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(submit).click().build().perform();	
	
	Thread.sleep(500);	
	
}

//************************************** Method to verify removed user ***************************************
public void Verify_Removed_users() throws InterruptedException
{
		Click_AddRemove();
	
		Thread.sleep(500);	
		
		String user= driver.findElement(By.id("P21_USERS_RIGHT")).getText();
		
		driver.navigate().back();
		
		Assert.assertEquals(user, "");
	
	}

//********************************************* Method to delete department ************************************
public void Delete_department() throws InterruptedException
{
	// click edit
		
	WebElement edit = driver.findElement(By.xpath("//tr[@class='even']//td[1]//a[1]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(edit).click().build().perform();	
	
	Thread.sleep(500);

	// Click create
	
	WebElement Create = driver.findElement(By.id("B8366324161092717"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(Create).click().build().perform();	
	
	Thread.sleep(500);
	
	//Accept alert
	
	Alert alert= driver.switchTo().alert();
	
	alert.accept();
}

//********************************************* Method to verify deleted department ***************************
public void VerifyDeleted() throws InterruptedException
{
	Thread.sleep(500);
	
	WebElement search= driver.findElement(By.id("apexir_SEARCH"));
	
	search.clear();
	
	search.sendKeys(Newname);
	
	//Click Go
	
	WebElement go = driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(go).click().build().perform();	
	
	Thread.sleep(500);
	
	String text= driver.findElement(By.xpath("//span[@id='apexir_NO_DATA_FOUND_MSG']")).getText();
	
	Assert.assertEquals(text, "No data found.");	
  
  }

}
