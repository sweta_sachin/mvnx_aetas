package SubscriberTests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Common_Methods.CommonMethods;

public class CustomerDetailsOfAssociatedSubscriber extends CommonMethods {
	
	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
		Thread.sleep(500);
	       	
	}
	
	@AfterClass(alwaysRun=true)
	public void end()
	{
		driver.close();
	}
	
	@Test
	public void a_View_CustomerDetailsTab() throws InterruptedException
	{
		Thread.sleep(800);
		view_customerDetailsTab();
	}
	
	@Test
	public void b_Click_CustomerDetailsTab() throws InterruptedException
	{
		Thread.sleep(800);
		Click_CustomerDetailsTab();
	}
	
	@Test
	public void c_View_CustomerDetails() throws InterruptedException
	{
		Thread.sleep(800);
		view_CustomerDetails();
	}
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//****************************** Method to view customer details tab for associated subscriber  ***************************
public void view_customerDetailsTab()
{

	WebElement SubServicesTab= driver.findElement(By.partialLinkText("Customer Details ID "));

	Boolean SubServiceTab= SubServicesTab.isDisplayed();

	Assert.assertTrue(SubServiceTab);

}
//******************************** Method to click customer details tab for associated subscriber ***************************
public void Click_CustomerDetailsTab()
{

	WebElement SubServicesTab= driver.findElement(By.partialLinkText("Customer Details ID"));

	Actions act= new Actions(driver);

	act.moveToElement(SubServicesTab).click().build().perform();

}

//****************************** Method to view customer details for associated subscriber  ***************************
public void view_CustomerDetails()
{

	WebElement service= driver.findElement(By.id("R13616970911260349"));

	Boolean ServiceDetails= service.isDisplayed();

	Assert.assertTrue(ServiceDetails);
}

}
