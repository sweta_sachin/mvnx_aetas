package Sales_Tab;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Merchant_Tab_tests extends Common_Methods.CommonMethods{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		click_SalesTab();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
	  driver.quit();			
	}
	
	@Test
	public void a_View_Merchant_Tab()
	{
		view_MerchantTab();
	}

	@Test
	public void b_Click_Merchant_Tab() throws InterruptedException
	{
		Thread.sleep(400);
		click_MerchantTab();
	}
	
	@Test
	public void c_Filter_report() throws InterruptedException
	{
		Thread.sleep(500);
		View_filtered_report();
	}
	
	@Test
	public void d__Save_Report() throws InterruptedException
	{
		Thread.sleep(500); 
		Save_report();
	}
	
	@Test
	public void e_Search_by_privateReport() throws InterruptedException
	{
		Thread.sleep(500); 
		search_by_private_report();
	}
	
	@Test
	public void f_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(300);
		search_by_PrimaryReports();
	}
	
		
	@Test
	public void h_Create_merchant() throws InterruptedException
	{
		Thread.sleep(300);
		Create_Merchant();
	}
	
	@Test
	public void i_Verify_Created() throws InterruptedException
	{
		Thread.sleep(300);
		Verify_Created();
	}
	
	@Test
	public void j_Edit_Merchant() throws InterruptedException
	{
		Thread.sleep(300);
		Edit_Merchant();
		
	}
	
	@Test
	public void k_Verify_Edit() throws InterruptedException
	{
		Thread.sleep(300);
		Verify_edited();
	}
	
	@Test
	public void l_Delete_Merchant() throws InterruptedException
	{
		Thread.sleep(300); 
		Delete_merchant();
	}
	
	@Test
	public void m_Verify_Deletion() throws InterruptedException
	{
		Thread.sleep(300);
		Verify_Deletion();
	}
	
	@Test
	public void n_reset_test() throws InterruptedException
	{
		Thread.sleep(1000);
		reset();
	} 
	
	@Test
	public void o_flashBackTest() throws InterruptedException
	{
		Thread.sleep(1000);
		flashback_filter();
	}
	
	
	@Test
	public void p_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","merchant.csv"));
	}
	
	@Test
	public void q_DownloadHTML() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_HTML("C:\\Users\\Sweta\\Downloads","merchant.htm"));
	}
	
	
	@Test
	public void r_emailReport() throws InterruptedException
	{
		Thread.sleep(500);
		email_report();
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Merchant tab ******************************
public void view_MerchantTab()
{
	WebElement Merchant= driver.findElement(By.linkText("Merchant"));
	
	Boolean merchant= Merchant.isDisplayed();
	
	Assert.assertTrue(merchant);
}


//********************************* Method to click Merchant tab ******************************
public void click_MerchantTab() throws InterruptedException
{
	WebElement Merchant= driver.findElement(By.linkText("Merchant"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(Merchant).click().build().perform();
	
	Thread.sleep(400);

	//Verify report
	
	WebElement verify= driver.findElement(By.id("apexir_SEARCH"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}


//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
{
	col= "External Reference";
	expvalue= "Test";
	
	view_filtered_report();
	
	// 	Verify report
	
	Thread.sleep(600);
	
	String verifyreport= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[4]")).getText();
	
	Thread.sleep(300);
	
	reset();
	
	Assert.assertEquals(verifyreport, expvalue);
	
}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "1. Test report"; 

	save_report();
	
	//click apply
	
	Click_apply();
	
	// Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();
	
	// verify 
	
	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
	
}

//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
{
	select_primary_report();
	
	Thread.sleep(300);
	
	try {
	
		WebElement primaryreport= driver.findElement(By.id("36418817776992319"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	
	}catch(Exception e) 	{
		
		WebElement primaryreport= driver.findElement(By.id("36418817776992319"));
	
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}
	
}
//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
{
	Thread.sleep(200);
	
	Reset();
	
	// Verify report
	
	WebElement primaryreport= driver.findElement(By.id("36418817776992319"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
	
}

//************************************ Method to create Merchant **********************************
public void Create_Merchant() throws InterruptedException
{
	//click create 

	WebElement create= driver.findElement(By.id("B36420230710992333"));
	
	Actions act0= new Actions(driver);
	
	act0.moveToElement(create).click().build().perform();
	
	Thread.sleep(300);
	
	//Select merchant
	
	Select merchant= new Select(driver.findElement(By.id("P19_MERC_D_UID")));
	
	merchant.selectByIndex(2);
	
	Thread.sleep(100);
	
	//Enter merchant code
	
	WebElement code= driver.findElement(By.id("P19_MERC_CODE"));
	
	code.clear();
	
	code.sendKeys(Name1);
		
	//Enter external reference
	
	WebElement ERef= driver.findElement(By.id("P19_MERC_EXTERNAL_REFERENCE"));
	
	ERef.clear();
	
	ERef.sendKeys(Name);
	
	//click create
	
	WebElement Create= driver.findElement(By.id("B36407108044394081"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(Create).click().build().perform();
	
	Thread.sleep(500);
	
	//Check success msg
	
	String Msg= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[2]/div[1]")).getText();
	
	String Expected_msg= "Merchant Created/SavedAction Processed.";
	
	Assert.assertEquals(Msg, Expected_msg);
   
}

//***************************************************** Method to verify created Merchant *************************************************
public void Verify_Created() throws InterruptedException
{
	Reset();
	
	//enter name to search

	WebElement search= driver.findElement(By.id("apexir_SEARCH"));
	
	search.clear();
	
	search.sendKeys(Name);
	
	Thread.sleep(200);
			
	//Click Go
	
	WebElement go= driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(go).click().build().perform();
	
	Thread.sleep(400);
	
	//verify search
	
	String text1= driver.findElement(By.xpath("//tr[2]//td[4]")).getText();
	
	Assert.assertEquals(text1, Name);
	
  }

//**************************************************** Method to Edit Merchant ***************************************************
public void Edit_Merchant() throws InterruptedException
{
	//Click edit
		
	WebElement edit= driver.findElement(By.xpath("//tr[2]//td[1]//a[1]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(edit).click().build().perform();
	
	Thread.sleep(300);
	
	//Change external ref
	
	WebElement ref= driver.findElement(By.id("P19_MERC_EXTERNAL_REFERENCE"));
	
	ref.clear();
	
	ref.sendKeys(Name1);
	
	//Click Apply changes
	
	WebElement apply= driver.findElement(By.id("B36407229712394081"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(apply).click().build().perform();
	
	Thread.sleep(300);

	// Check success message
	
	Success_Msg();
	
  }
//******************************************** Method to verify edited merchant**********************************
public void Verify_edited() throws InterruptedException
{
		
	//reset all filters
	
	reset();
	
	Thread.sleep(800);
	 	
	//enter name to search
	
	WebElement search= driver.findElement(By.id("apexir_SEARCH"));
	
	search.clear();
	
	search.sendKeys(Name1);
	
	Thread.sleep(500);
		
	//Click Go
	
	WebElement go= driver.findElement(By.id("apexir_btn_SEARCH"));
	
	Actions act2= new Actions(driver);
	
	act2.moveToElement(go).click().build().perform();
	
	Thread.sleep(400);
		
	//verify search
	
	String text1= driver.findElement(By.xpath("//tr[2]//td[4]")).getText();
	
	Assert.assertEquals(text1, Name1);

}

//************************************************** Method to delete merchant ********************************************
public void Delete_merchant() throws InterruptedException
{
	//Click edit

	WebElement edit= driver.findElement(By.xpath("//tr[2]//td[1]//a[1]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(edit).click().build().perform();
	
	Thread.sleep(700);
		
	//Click delete
	
	WebElement delete= driver.findElement(By.id("B36407312592394081"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(delete).click().build().perform();
	
	Thread.sleep(300);
			
	//Accept alert
	
	Alert alert= driver.switchTo().alert();
	
	alert.accept();

	// Check success message
	
	Success_Msg();
  
}

//*********************************************** Verify Deletion *******************************
public void Verify_Deletion() throws InterruptedException
{
	//reset all filters
			
	Reset();
			
	Thread.sleep(500);
	
	//enter name to search
	
	WebElement search= driver.findElement(By.id("apexir_SEARCH"));
			
	search.clear();
			
	search.sendKeys(Name1);
			
	Thread.sleep(500);
			
	//Click Go
	
	WebElement go= driver.findElement(By.id("apexir_btn_SEARCH"));
			
	Actions act2= new Actions(driver);
			
	act2.moveToElement(go).click().build().perform();
			
	Thread.sleep(400);
						
	//verify search
	
	Boolean text1= driver.findElement(By.xpath("//span[@id='apexir_NO_DATA_FOUND_MSG']")).isDisplayed();
	
	Assert.assertTrue(text1);
  }
}
