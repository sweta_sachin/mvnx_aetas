package SubscriberTestsOnLeftHandMenu;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Common_Methods.quickNoteTests;

public class QiuckNotesForSubscriber_FromLeftHandMenu extends quickNoteTests{
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
		Thread.sleep(500);
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
        Thread.sleep(1000);
		driver.quit();
	}
	
	@Test
	public void a_Click_QuickNotesFor_Subscriber_FromLeftHandMenu() throws InterruptedException
	{
		click_QuickNotes();		
	}
	
	@Test
	public void b_Click_Create_Quicknote_FromLeftHandMenu() throws InterruptedException
	{
		
		ClickCreateQuickNote();
		Thread.sleep(1000);
	}
	
	@Test
	public void c_Select_type_FromLeftHandMenu() throws InterruptedException
	{
		Thread.sleep(1000);
		Select_Type();
	}
	
	@Test
	public void d_Enter_Subject_FromLeftHandMenu()
	{
		Enter_subject();
	}
	
	@Test
	public void e_Enter_Note_FromLeftHandMenu()
	{
		
		Enter_note();
	}
	
	@Test
	public void f_Select_Status_FromLeftHandMenu()
	{
		Select_status();
	}
	
	@Test
	public void g_Select_FaultType_FromLeftHandMenu()
	{
	//	Select_faultType();
	}
	
	@Test
	public void h_Click_Create_FromLeftHandMenu()
	{
		Click_create();
	}
	
	@Test
	public void i_check_success_message_FromLeftHandMenu()
	{
		suceess_msg();
	}
	
	@Test
	public void la_VerifyCreatedNote() throws InterruptedException 
	{
		Thread.sleep(1000);
		Verify_note();
		
	}
	
//************************** Method to click Quick Notes Tab for a subscriber ********************
public void click_QuickNotes() throws InterruptedException
{
	WebElement QuickNotes= driver.findElement(By.xpath("//ul[@class='ui-helper-clearfix']//a[contains(text(),'Quick Notes')]"));
	
	Actions act= new Actions(driver);
	
	Thread.sleep(1500);
	
	act.moveToElement(QuickNotes).doubleClick().build().perform();
	
	Thread.sleep(1500);
  }
}
