package TestsOnLeftHandTabsOnCRM;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Common_Methods.quickNoteTests;

public class NotesOnLeftHandMenu extends quickNoteTests {

	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		SearchCustomerByAccountNumber(AccNo);
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		clickNotes();
	}
	
	@AfterClass(alwaysRun=true)
	public void end()
	{
		driver.close();
	}

	@Test
	public void a_Click_Create_Quicknote() throws InterruptedException
	{
		Thread.sleep(1000);
		ClickCreateQuickNote();
	}
	
	@Test
	public void b_Select_type()
	{
		Select_Type();
	}
	
	@Test
	public void c_Enter_Subject()
	{
		Enter_subject();
	}
	
	@Test
	public void d_Enter_Note()
	{
		
		Enter_note();
	}
	
	@Test
	public void e_Select_Status()
	{
		Select_status();
	}
	
	@Test
	public void f_Select_FaultType()
	{
		Select_faultType();
	}
	
	@Test
	public void g_Click_Create()
	{
		Click_create();
	}
	@Test
	public void i_check_success_message()
	{
		suceess_msg();
	}
	
	@Test
	public void la_VerifyCreatedNote() throws InterruptedException 
	{
		Thread.sleep(1000);
		Verify_note();
		
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to click quick notes **********************************
public void clickNotes() throws InterruptedException
{
	try {
		  Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
				.withTimeout(30, TimeUnit.SECONDS) 			
				.pollingEvery(5, TimeUnit.SECONDS) 			
				.ignoring(NoSuchElementException.class);
			
		  WebElement QuickNotes= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/ul[1]/li[7]/a[1]"));
		  
		  Boolean name= QuickNotes.isDisplayed();
		  
		  if(name==true)  {
		   
			  wait.equals(name);
		    
			  Actions act= new Actions(driver);
		    
			  Thread.sleep(1500);
			
			  act.moveToElement(QuickNotes).click().build().perform();
			
			  Thread.sleep(1500);
		    }
		  
	}catch(StaleElementReferenceException e)    {
	
		WebElement QuickNotes= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/ul[1]/li[7]/a[1]"));
		
		Actions act= new Actions(driver);
		
		Thread.sleep(1500);
		
		act.moveToElement(QuickNotes).click().build().perform();
		
		Thread.sleep(1500);
	}
  }
}
