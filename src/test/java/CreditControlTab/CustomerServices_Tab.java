package CreditControlTab;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CustomerServices_Tab extends Common_Methods.CommonMethods{
	
	public static String ContactName= "name";
	
	public static String Sname= "add service name once data is available";
	
	public static String DATE1= "13-Nov-2019";
	
	public static String ACName= "add account name once data is available";

			
	@BeforeClass()
	public void CRM() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		clickCreditControlTab() ;	
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() 
	{
	  driver.quit();
	}
	
	@Test
	public void a_view_CustomerService_Tab()
	{
		view_CustomerServiceTab();
	}
	
	@Test
	public void b_Click_CustomerService_Tab() throws InterruptedException
	{
		Thread.sleep(500);
		click_CustomerServiceTab();
	}
	
	@Test
	public void c_Search_by_AccountNumber() throws InterruptedException
	{
		Thread.sleep(300);
		Search_by_account_number();
	}
		
	@Test
	public void d_Search_by_AccountName() throws InterruptedException
	{
		Thread.sleep(300);
		Search_by_account_name();
	}
	
	@Test
	public void e_Search_by_Services() throws InterruptedException
	{
		Thread.sleep(300);
		Search_by_services();
	}
	
	@Test
	public void f_Search_by_ratePlan() throws InterruptedException
	{
		Thread.sleep(300);
		Search_by_ratePlan();
	}
	
	@Test
	public void g_General_Seach() throws InterruptedException
	{
		Thread.sleep(300);
		General_search();
	}
	
	@Test
	public void h_Filter_report() throws InterruptedException
	{
		Thread.sleep(500);
		View_filtered_report();
	}
	
	@Test
	public void i__Save_Report() throws InterruptedException
	{
		Thread.sleep(500); 
		Save_report();
	}
	
	@Test
	public void j_Search_by_privateReport() throws InterruptedException
	{
		search_by_private_report();
	}
	
	@Test
	public void k_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(300);
		search_by_PrimaryReports();
	}
	
	@Test
	public void m_reset_test() throws InterruptedException
	{
		Thread.sleep(300);
		reset();
	} 
		
	@Test
	public void ma_Subscdription_email() throws InterruptedException
	{
		Thread.sleep(3000);
		get_subscription();
	}
	
	
	@Test
	public void n_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  DownLoad_CSV());
	}
	
	@Test
	public void o_DownloadHTML() throws InterruptedException
	{
		Thread.sleep(800);
		Assert.assertTrue(  DownLoad_HTML());
	}
	
	@Test
	public void p_emailReport() throws InterruptedException
	{
		Thread.sleep(300);
		email_report();
	}
	
	@Test
	public void q_Download_pdf() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue( DownLoad_PDF());
	}
	
	@Test
	public void s_View_Customer_Details() throws InterruptedException
	{
		Thread.sleep(1300);
		view_customerDetails();
	}
	
	@Test
	public void t_Edit_Customer_Details() throws InterruptedException
	{
		Thread.sleep(1300);
		edit_customerDetails();
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Customer Service tab ******************************
public void view_CustomerServiceTab()
{
	WebElement C_Service= driver.findElement(By.linkText("Customer Services"));
	
	Boolean Service= C_Service.isDisplayed();
	
	Assert.assertTrue(Service);
}

//********************************* Method to click Customer Service tab ******************************
public void click_CustomerServiceTab() throws InterruptedException
{
	WebElement C_Service= driver.findElement(By.linkText("Customer Services"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(C_Service).click().build().perform();
	
	Thread.sleep(300);
	
	WebElement verify= driver.findElement(By.id("P50_ACC_NO"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}		

//********************************* Method to click Search *****************************************
public void click_search() throws InterruptedException
{
	WebElement search= driver.findElement(By.id("P50_SEARCH"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(search).click().build().perform();
	
	Thread.sleep(300);
}

//********************************** Method to search by account number *****************************
public void Search_by_account_number() throws InterruptedException
{
	// enter account number

	WebElement AcNumber= driver.findElement(By.id("P50_ACC_NO"));
	
	AcNumber.clear();
	
	AcNumber.sendKeys(AccNo);
	
	Thread.sleep(300);
	
	click_search();
	
	Thread.sleep(300);

	//Verify report
	
	driver.findElement(By.id("P50_ACC_NO")).clear();
	
	String text= driver.findElement(By.xpath("//div[@id='R32713525342861231']//td[2]")).getText();
	
	Assert.assertEquals(text, AccNo);
	 
	 }

//********************************** Method to search by account name *****************************

public void Search_by_account_name() throws InterruptedException
  {
	// enter account name
	
	WebElement AcName= driver.findElement(By.id("P50_ACC_NAME"));
	
	AcName.clear();
	
	AcName.sendKeys(ACName);
	
	Thread.sleep(300);
	
	click_search();
	
	Thread.sleep(300);
		
	//Verify report
	
	driver.findElement(By.id("P50_ACC_NAME")).clear();
	
	String text= driver.findElement(By.xpath("//div[@id='R32713525342861231']//td[3]")).getText();
	
	Assert.assertEquals(text, ACName);
	
  }

//********************************** Method to search by services *****************************
public void Search_by_services() throws InterruptedException
{
	// select service
	
	Select service= new Select(driver.findElement(By.id("P50_SRV_UID")));
	
	service.selectByVisibleText(Sname);
	
	Thread.sleep(200);
	
	click_search();
	
	Thread.sleep(300);
	 
	 //Verify report
	
	Select service1= new Select(driver.findElement(By.id("P50_SRV_UID")));
	
	service1.selectByVisibleText("-- Select --");
	
	String text= driver.findElement(By.xpath("//div[@id='R32713525342861231']//td[7]")).getText();
	
	Assert.assertEquals(text, Sname);
	 
 }

//********************************** Method to search by rate plan *****************************
public void Search_by_ratePlan() throws InterruptedException
{
	// select rate plan
		
	Select Rplan= new Select(driver.findElement(By.id("P50_RP_UID")));
	
	Rplan.selectByIndex(3);
	
	WebElement plan= Rplan.getFirstSelectedOption();
	
	String PLAN= plan.getText();
	
	Thread.sleep(200);
	
	click_search();
	
	Thread.sleep(300);
	 
	//Verify report
	
	Select Rplan1= new Select(driver.findElement(By.id("P50_RP_UID")));
	
	Rplan1.selectByVisibleText("-- Select --");
	
	String text= driver.findElement(By.xpath("//div[@id='R32713525342861231']//td[24]")).getText();
	
	Assert.assertEquals(text, PLAN);
	 
 }

//********************************** Method to do general search without any filter *****************************

public void General_search() throws InterruptedException
{	
	click_search();

	Thread.sleep(300);
	
	//Verify report
    
	WebElement primaryreport= driver.findElement(By.id("32713711641861233"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
   }

//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
{
	expvalue= "CYC01";	
	col= "Bill Cycle";
	
	view_filtered_report();

	//Verify report
	
	Thread.sleep(400);
	
	String verifyreport= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[2]/div[2]/div[2]/div[2]/div[2]/div[2]/div[4]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[2]/td[6]")).getText();
	
	Thread.sleep(300);
	
	Assert.assertEquals(verifyreport, expvalue);
	
}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "1. Test report"; 

	save_report();
	 	
	// Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
	 	 
}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();	
		
	// verify 
	
	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
	
}

//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
{
	try {
			select_primary_report();
			
			Thread.sleep(300);
			
			WebElement primaryreport= driver.findElement(By.id("32713711641861233"));
			
			Boolean report= primaryreport.isDisplayed();
			
			Assert.assertTrue(report);
		
	} catch(Exception e){
	
		select_primary_report();
		
		Thread.sleep(300);
		
		WebElement primaryreport= driver.findElement(By.id("32713711641861233"));
		
		Boolean report= primaryreport.isDisplayed();
		
		Assert.assertTrue(report);
	}
}

//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
{
	Thread.sleep(200);

	Reset();
	 
	// Verify report
	
	WebElement primaryreport= driver.findElement(By.id("32713711641861233"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
}
//*************************************** Method to view customer details **********************
public void view_customerDetails() throws InterruptedException
{
	// Click on account number to view customer details
	
	Thread.sleep(400);
	
	WebElement account_number = driver.findElement(By.xpath("//div[@id='R32713525342861231']//tr[2]//td[2]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(account_number).click().build().perform();	
	
	Thread.sleep(500);
	 
	// Verify 
	
	WebElement Account_Details= driver.findElement(By.xpath("//a[contains(text(),'All Subscribers for Account')]"));
	
	Boolean report= Account_Details.isDisplayed();
	
	driver.navigate().back();
	
	Assert.assertTrue(report);
 }

//***************************************** Method to edit customer details **********************
public void edit_customerDetails() throws InterruptedException
{
	 // Click on edit icon to edit customer details
	
	Thread.sleep(500);
	
	WebElement edit = driver.findElement(By.xpath("//div[@id='R32713525342861231']//tr[2]//td[1]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(edit).click().build().perform();	
	
	Thread.sleep(800);
		 
	// Edit registration date, bill cycle and contact name
	
	// Edit date 
	
	WebElement date= driver.findElement(By.id("P40_BCD_COMPANY_REG_DATE"));
	
	date.clear();
	
	date.sendKeys(DATE1);
	
	Thread.sleep(300);

	//Edit bill run 	 
	
	Select billrun = new Select(driver.findElement(By.id("P40_BCD_BBC_UID")));
	
	billrun.selectByIndex(2);
	
	Thread.sleep(300);
		
	// Edit account name
	
	WebElement contact_name= driver.findElement(By.id("P40_BCD_NAME"));
	
	contact_name.clear();
	
	contact_name.sendKeys(ContactName);
	
	Thread.sleep(300);
		 
	// Click apply changes
	
	WebElement apply = driver.findElement(By.id("B13363654567673223"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(apply).click().build().perform();	
	
	Thread.sleep(500); 
		 
	//Verify success message
	
	String Msg= driver.findElement(By.id("echo-message")).getText();
	
	Boolean msg=Msg.endsWith("fields updated.");
	
	Assert.assertTrue(msg);
	
	Thread.sleep(800); 
		
	//click customer details on breadcrumbs
	
	WebElement cust_details= driver.findElement(By.xpath("//a[contains(text(),'Customer Details')]"));
	
	Actions act2 = new Actions(driver);
	
	act2.moveToElement(cust_details).click().build().perform();
		
	
	/*	Thread.sleep(1300);
		String date2= driver.findElement(By.id("P40_BCD_COMPANY_REG_DATE")).getText();
		Assert.assertEquals(date2, DATE1);
		*/
	
	//Verify Contact name
	
	Thread.sleep(800);
	
	String name1= driver.findElement(By.id("P2_BCD_NAME")).getText();
	
	System.out.println(name1);
	
	Assert.assertEquals(name1, ContactName);
	
}

//*********************************** Method to test download HTML ***************************
public boolean DownLoad_PDF() throws InterruptedException
{
	Thread.sleep(200);
	
 // Choose and click PDF report type to download 

	Thread.sleep(500);
	
	WebElement pdf = driver.findElement(By.id("apexir_dl_PDF"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(pdf).click().build().perform();
	
	Thread.sleep(1500);
	
	String folderName = "C:\\Users\\Sweta\\Downloads"; // Give your folderName
	
	File[] listFiles = new File(folderName).listFiles();
	
	for (int i = 0; i < listFiles.length; i++) {
	
		if (listFiles[i].isFile()) {
		
			String filename = listFiles[i].getName();
		    
			if (filename.startsWith("customeradmin")
		    					&& filename.endsWith(".pdf")) {
		            
					System.out.println("found file" + " " + filename);
		            
					listFiles[i].delete();
		            
					return true;
		        }
	          }
		   	}
		
	return false;
}	

//*********************************** Method to test download HTML ***************************
public boolean DownLoad_HTML() throws InterruptedException
{

	Thread.sleep(200);

	// Choose and click HTML report type to download 
	
	Thread.sleep(500);
	
	WebElement html = driver.findElement(By.id("apexir_dl_HTML"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(html).click().build().perform();
	
	Thread.sleep(5000);
	
	String folderName = "C:\\Users\\Sweta\\Downloads"; // Give your folderName
	
	File[] listFiles = new File(folderName).listFiles();
	
	for (int i = 0; i < listFiles.length; i++) {
	
		if (listFiles[i].isFile()) {
		
			String filename = listFiles[i].getName();
			
			if (filename.startsWith("customeradmin")
							&& filename.endsWith(".htm")) {
						
				Thread.sleep(3500);
				
				System.out.println("found file" + " " + filename);
				
				listFiles[i].delete();
				
				return true;
			}
	    }
	}
		return false;
}


//*********************************** Method to test download CSV ***************************
public boolean DownLoad_CSV() throws InterruptedException
{
	click_ActionButton();
	
	Thread.sleep(200);
	
	//Click download option
		
	WebElement reset = driver.findElement(By.xpath("//a[contains(text(),'Download')]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(reset).click().build().perform();
	 
	 // Choose and click CSV report type to download 
	
	Thread.sleep(1000);
	
	WebElement csv = driver.findElement(By.xpath("//a[@id='apexir_dl_CSV']//img"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(csv).click().build().perform();
	
	Thread.sleep(1500);
	
	String folderName = "C:\\Users\\Sweta\\Downloads"; // Give your folderName
	
	File[] listFiles = new File(folderName).listFiles();
	
	for (int i = 0; i < listFiles.length; i++) {
	
		if (listFiles[i].isFile()) {
	 	
			String filename = listFiles[i].getName();
		    
			if (filename.startsWith("customeradmin")
		        		&& filename.endsWith(".csv")) {
		        		
				Thread.sleep(2500);
		        
				System.out.println("found file" + " " + filename);
		        
				listFiles[i].delete();
		        
				return true;
			}
		}
	}
		return false;
    }
}
