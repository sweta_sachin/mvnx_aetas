package CreditControlTab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Direct_debit_tab extends Common_Methods.CommonMethods{
	
   	@BeforeClass()
	public void CRM() throws InterruptedException
	{
		Login();
		Thread.sleep(500);
		clickCreditControlTab() ;	
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() 
	{
	    driver.quit();
	}
	
	@Test
	public void a_view_DirectDebit_Tab()
	{
		view_directDebitTab();
	}
	
	@Test
	public void b_Click_DirectDebit_Tab() throws InterruptedException
	{
		Thread.sleep(500);
		click_directDebitTab();
		
	}
	
	@Test
	public void c_Filter_report() throws InterruptedException
	{
		Thread.sleep(500);
		View_filtered_report();
	}
	
	@Test
	public void d_Save_Report() throws InterruptedException
	{
		Thread.sleep(500); 
		Save_report();
	}
	
	@Test
	public void e_Search_by_privateReport() throws InterruptedException
	{
		search_by_private_report();
	}
	
	@Test
	public void ba_search_By_Primary_report() throws InterruptedException
	{
		Thread.sleep(300);
		search_by_PrimaryReports();
	}
	
	@Test
	public void i_DownloadCSV() throws InterruptedException
	{
		Thread.sleep(300);
		Assert.assertTrue(  Download_CSV("C:\\Users\\Sweta\\Downloads","direct_debit_file_summary.csv"));
	}
	
	@Test
	public void j_Send_Subscription_Email() throws InterruptedException
	{
		Thread.sleep(300);
		get_subscription();
		
	}
	
	@Test
	public void k_flashBackTest() throws InterruptedException
	{
		Thread.sleep(300);
		flashback_filter();
	}
	
	@Test
	public void l_reset_test() throws InterruptedException
	{
		Thread.sleep(300);
		reset();
	}
	
	@Test
	public void m_viewFile() throws InterruptedException
	{
		Thread.sleep(300);
		
		viewFile();
	}
	
	@Test
	public void n_ViewAccountDetails_byClicking_OnAccountNumber() throws InterruptedException
	{
		Thread.sleep(300);
		View_AccountDetails();		
	}
	
	@Test
	public void o_Click_ManageDD_option() throws InterruptedException
	{
		Thread.sleep(300);
		ManageDD();
	}
	
	@Test
	public void p_Change_NextRunDateInManageDD() throws InterruptedException
	{
		Thread.sleep(300);
		ChangeNextRunDate();
	}

	
	
//////////////////////////////////////////////////////////////////////////////////////////////
//********************************* Method to view Direct Debit tab ******************************
public void view_directDebitTab()
{
	WebElement aging= driver.findElement(By.linkText("Direct Debits"));
	
	Boolean Aging= aging.isDisplayed();
	
	Assert.assertTrue(Aging);
}

//********************************* Method to click Direct Debit tab ******************************
public void click_directDebitTab()
{
	WebElement aging= driver.findElement(By.linkText("Direct Debits"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(aging).click().build().perform();
	
	WebElement verify= driver.findElement(By.xpath("//a[@id='B3015422279680305']"));
	
	Boolean Verify= verify.isDisplayed();
	
	Assert.assertTrue(Verify);
}

//************************************** Method to apply filter and view filtered report **********************
public void View_filtered_report() throws InterruptedException
{
	 expvalue= "Outbound";
	 col= "File Type";
	
	 view_filtered_report();
	
		try {
	
			// Verify report
			
			Thread.sleep(300);
			
			String verifyreport= driver.findElement(By.xpath("//tr[3]//td[4]")).getText();
			
			Assert.assertEquals(verifyreport, expvalue);
			
		}catch(Exception e)	{
		
			// Verify report
			
			Thread.sleep(300);
			
			String verifyreport= driver.findElement(By.xpath("//tr[3]//td[4]")).getText();
			
			Assert.assertEquals(verifyreport, expvalue);	
		}
	}

//************************************* Method to save report *************************************
public void Save_report() throws InterruptedException
{
	report= "1. Test report"; 
	
	save_report();
	
	 // Verify saved report
	
	Thread.sleep(500);
	
	Select dropdown= new Select(driver.findElement(By.id("apexir_SAVED_REPORTS")));
	
	dropdown.selectByVisibleText(report);
	 	 
	}

//**************************************** Method to search by private report ****************************
public void search_by_private_report() throws InterruptedException
{
	Search_by_private_report();	
	
	// verify 
	
	Thread.sleep(300);
	
	String text= driver.findElement(By.partialLinkText("Saved Report = ")).getText();
	
	System.out.println(text);
	
	Assert.assertEquals(text, verifyreport);
	
	}
//*********************************** Method to search primary report *********************
public void search_by_PrimaryReports() throws InterruptedException
{
	select_primary_report();
	
	Thread.sleep(300);
	
	WebElement primaryreport= driver.findElement(By.id("3226900314155754"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
}

//************************ Method to test reset button on CIB *******************
public void reset() throws InterruptedException
{
	Reset();
	
	Thread.sleep(400);
	 
	// Verify report
	
	WebElement primaryreport= driver.findElement(By.id("3226900314155754"));
	
	Boolean report= primaryreport.isDisplayed();
	
	Assert.assertTrue(report);
	
}

//********************************** Method to view files by clicking on the view icon ***********************
public void viewFile()
   {
	// Click view Option 

	WebElement view = driver.findElement(By.xpath("//tr[3]//td[1]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(view).click().build().perform();
	 
	// Verify 
	
	WebElement files= driver.findElement(By.id("apexir_SS_NAME"));
	
	Boolean report= files.isDisplayed();
	
	Assert.assertTrue(report);	 
  }

//************************************** Method to view account details *************************
public void View_AccountDetails() throws InterruptedException
{
	// Click on account number
	
	WebElement AcNo = driver.findElement(By.xpath("//tr[3]//td[2]"));
	
	String accountNO= AcNo.getText();
	
	Actions act= new Actions(driver);
	
	act.moveToElement( AcNo).click().build().perform();
	
	Thread.sleep(400);
	
	String AccountNumber= driver.findElement(By.id("P2_BCD_ACC_NO")).getText();
	
	Assert.assertEquals(AccountNumber, accountNO);
	
	Thread.sleep(400);
	
	driver.navigate().back();
	
	driver.navigate().back();
  
}

//************************************* Method to click manage DD ****************************
public void ManageDD() throws InterruptedException
{
	//Click Manage DD option

	Thread.sleep(300);
	
	WebElement managedd = driver.findElement(By.id("B3015422279680305"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(managedd).click().build().perform();
	
	Thread.sleep(300);
	
	 // Verify 
	
	WebElement files= driver.findElement(By.id("3011706121408998"));
	
	Boolean report= files.isDisplayed();
	
	Assert.assertTrue(report);	 
	 
}

//*************************************** Method to change the next run date through manage DD ***************************
public void ChangeNextRunDate() throws InterruptedException
{
	// Click on run date
	
	WebElement date = driver.findElement(By.xpath("//tr[3]//td[4]"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement( date).click().build().perform();
	
	Thread.sleep(400);
		 
	// Choose or enter new date 
	
	WebElement newdate= driver.findElement(By.id("P17_DDC_NEXT_RUN_DATE"));
	
	newdate.clear();
	
	newdate.sendKeys(DATE);
	
	// Click submit button
	
	WebElement submit = driver.findElement(By.id("B2609719793217756"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(submit).click().build().perform();
	
	Thread.sleep(200);
	
	String NewDate= driver.findElement(By.xpath("//tr[3]//td[4]")).getText();
	
	Assert.assertEquals(NewDate, DATE);
	
	Thread.sleep(400);
   	
      }
}
